# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-01-30 12:41+0000\n"
"PO-Revision-Date: 2018-02-06 15:26+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"About\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"> **amnesia**, *noun*:<br/>\n"
"> forgetfulness; loss of long-term memory.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"> **incognito**, *adjective & adverb*:<br/>\n"
"> (of a person) having one's true identity concealed.\n"
msgstr ""

#. type: Plain text
msgid ""
"Tails is a live system that aims to preserve your privacy and anonymity. It "
"helps you to use the Internet anonymously and circumvent censorship almost "
"anywhere you go and on any computer but leaving no trace unless you ask it "
"to explicitly."
msgstr ""

#. type: Plain text
msgid ""
"It is a complete operating system designed to be used from a USB stick or a "
"DVD independently of the computer's original operating system. It is [[Free "
"Software|doc/about/license]] and based on [[Debian GNU/Linux|https://www."
"debian.org/]]."
msgstr ""

#. type: Plain text
msgid ""
"Tails comes with several built-in applications pre-configured with security "
"in mind: web browser, instant messaging client, email client, office suite, "
"image and sound editor, etc."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"tor\"></a>\n"
msgstr "<a id=\"tor\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Online anonymity and censorship circumvention\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tor\n"
"---\n"
msgstr ""

#. type: Plain text
msgid ""
"Tails relies on the Tor anonymity network to protect your privacy online:"
msgstr ""

#. type: Bullet: '  - '
msgid "all software is configured to connect to the Internet through Tor"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"if an application tries to connect to the Internet directly, the connection "
"is automatically blocked for security."
msgstr ""

#. type: Plain text
msgid ""
"Tor is an open and distributed network that helps defend against traffic "
"analysis, a form of network surveillance that threatens personal freedom and "
"privacy, confidential business activities and relationships, and state "
"security."
msgstr ""

#. type: Plain text
msgid ""
"Tor protects you by bouncing your communications around a network of relays "
"run by volunteers all around the world: it prevents somebody watching your "
"Internet connection from learning what sites you visit, and it prevents the "
"sites you visit from learning your physical location."
msgstr ""

#. type: Plain text
msgid "Using Tor you can:"
msgstr ""

#. type: Bullet: '  - '
msgid "be anonymous online by hiding your location,"
msgstr ""

#. type: Bullet: '  - '
msgid "connect to services that would be censored otherwise;"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"resist attacks that block the usage of Tor using circumvention tools such as "
"[[bridges|doc/first_steps/startup_options/bridge_mode]]."
msgstr ""

#. type: Plain text
msgid ""
"To learn more about Tor, see the official [Tor website](https://www."
"torproject.org/), particularly the following pages:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Tor overview: Why we need Tor](https://www.torproject.org/about/overview."
"html.en#whyweneedtor)"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Tor overview: How does Tor work](https://www.torproject.org/about/overview."
"html.en#thesolution)"
msgstr ""

#. type: Bullet: '- '
msgid "[Who uses Tor?](https://www.torproject.org/about/torusers.html.en)"
msgstr ""

#. type: Bullet: '- '
msgid ""
"[Understanding and Using Tor — An Introduction for the Layman](https://trac."
"torproject.org/projects/tor/wiki/doc/TorALaymansGuide)"
msgstr ""

#. type: Plain text
msgid ""
"To learn more about how Tails ensures all its network connections use Tor, "
"see our [[design document|contribute/design/Tor_enforcement]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"amnesia\"></a>\n"
msgstr "<a id=\"amnesia\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Use anywhere but leave no trace\n"
msgstr ""

#. type: Plain text
msgid ""
"Using Tails on a computer doesn't alter or depend on the operating system "
"installed on it. So you can use it in the same way on your computer, a "
"friend's computer, or one at your local library. After shutting down Tails, "
"the computer will start again with its usual operating system."
msgstr ""

#. type: Plain text
msgid ""
"Tails is configured with special care to not use the computer's hard-disks, "
"even if there is some swap space on them. The only storage space used by "
"Tails is in RAM, which is automatically erased when the computer shuts down. "
"So you won't leave any trace on the computer either of the Tails system "
"itself or what you used it for. That's why we call Tails \"amnesic\"."
msgstr ""

#. type: Plain text
msgid ""
"This allows you to work with sensitive documents on any computer and "
"protects you from data recovery after shutdown. Of course, you can still "
"explicitly save specific documents to another USB stick or external hard-"
"disk and take them away for future use."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"cryptography\"></a>\n"
msgstr "<a id=\"cryptography\"></a>\n"

#. type: Title =
#, no-wrap
msgid "State-of-the-art cryptographic tools\n"
msgstr ""

#. type: Plain text
msgid ""
"Tails also comes with a selection of tools to protect your data using strong "
"encryption:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Encrypt your USB sticks or external hard-disks|doc/encryption_and_privacy/"
"encrypted_volumes]] using <span class=\"definition\">[[!wikipedia "
"Linux_Unified_Key_Setup desc=\"LUKS\"]]</span>, the Linux standard for disk-"
"encryption."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Automatically use HTTPS to encrypt all your communications to a number of "
"major websites using [HTTPS Everywhere](https://www.eff.org/https-"
"everywhere), a Firefox extension developed by the [Electronic Frontier "
"Foundation](https://www.eff.org)."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Encrypt and sign your emails and documents using the *de facto* standard "
"<span class=\"definition\">[[!wikipedia Pretty_Good_Privacy#OpenPGP desc="
"\"OpenPGP\"]]</span> either from Tails email client, text editor or file "
"browser."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Protect your instant messaging conversations using <span class=\"definition"
"\">[[!wikipedia Off-the-Record_Messaging desc=\"OTR\"]]</span>, a "
"cryptographic tool that provides encryption, authentication and deniability."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Securely delete your files|doc/encryption_and_privacy/secure_deletion]] "
"and clean your diskspace using [[Nautilus Wipe|http://wipetools.tuxfamily."
"org/nautilus-wipe.html]]."
msgstr ""

#. type: Plain text
msgid ""
"[[Read more about those tools in the documentation.|doc/"
"encryption_and_privacy]]"
msgstr ""

#. type: Title =
#, no-wrap
msgid "What's next?\n"
msgstr ""

#. type: Plain text
msgid "To continue discovering Tails, you can now read:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"the [[warning page|doc/about/warning]] to better understand the security "
"limitations of Tails and Tor,"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"more details about the [[features and software|doc/about/features]] included "
"in Tails,"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"our [[installation instructions|install]] to download and install Tails,"
msgstr ""

#. type: Bullet: '  - '
msgid "our [[documentation|doc]] explaining in detail how to use Tails,"
msgstr ""

#. type: Bullet: '  - '
msgid "some hints on why [[you should trust Tails|doc/about/trust]],"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"our [[design document|contribute/design]] laying out Tails specification, "
"threat model and implementation,"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"the [[calendar|contribute/calendar]] that holds our release dates, meetings "
"and other events."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Press and media\n"
msgstr ""

#. type: Plain text
msgid "See [[Press and media information|press]]."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Social Contract\n"
msgstr ""

#. type: Plain text
msgid ""
"Read our [[Social Contract|contribute/working_together/social_contract]]."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Acknowledgments and similar projects\n"
msgstr ""

#. type: Plain text
msgid ""
"See [[Acknowledgments and similar projects|doc/about/"
"acknowledgments_and_similar_projects]]."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Contact\n"
msgstr ""

#. type: Plain text
msgid "See the [[contact page|about/contact]]."
msgstr ""
