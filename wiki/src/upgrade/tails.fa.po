# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-10-31 11:29+0000\n"
"PO-Revision-Date: 2019-10-24 10:32+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: \n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Manually upgrade from your Tails\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/upgrade-tails\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"hidden-step-1\"></div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/os-tails.png link=\"no\" alt=\"\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p class=\"start\">Start in Tails.</p>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Install an intermediary Tails\n"
msgstr ""

#. type: Plain text
msgid ""
"In this step, you will install an intermediary Tails using the Tails USB "
"image that you downloaded earlier."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "   [[!inline pages=\"news/version_3.5/manual_upgrade.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgid "[[!inline pages=\"install/inc/steps/install_with_gnome_disks.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_with_gnome_disks.inline.fa\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.fa\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/clone.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/clone.inline.fa\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "<div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#~ msgid "</div>\n"
#~ msgstr "</div>\n"
