# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-08-29 08:06+0000\n"
"PO-Revision-Date: 2018-07-02 10:58+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Warnings about persistence\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"sensitive_documents\"></a>\n"
msgstr "<a id=\"sensitive_documents\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Storing sensitive documents\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"**The persistent volume is not hidden.** An attacker in possession of\n"
"your USB stick can know that there is a persistent volume on it. Take into consideration\n"
"that you can be forced or tricked to give out its passphrase.\n"
msgstr ""

#. type: Plain text
msgid ""
"Read also our instructions to [[securely delete the persistent volume|"
"delete]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"overwriting_configurations\"></a>\n"
msgstr "<a id=\"overwriting_configurations\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Overwriting configurations\n"
msgstr ""

#. type: Plain text
msgid ""
"The programs included in Tails are carefully configured with security in "
"mind. If you use the persistence volume to overwrite the configuration of "
"the programs included in Tails, it can break this security or render these "
"programs unusable."
msgstr ""

#. type: Plain text
msgid ""
"Be especially careful when using the [[Dotfiles|persistence/"
"configure#dotfiles]] feature."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Furthermore, the anonymity of Tor and Tails relies on making it harder to\n"
"distinguish one Tails user from another. <strong>Changing the default\n"
"configurations can break your anonymity.</strong>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"minimum\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Use to the minimum\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"**Use the persistent volume only when necessary and to the minimum.** It is\n"
"always possible to start Tails without activating the persistent volume. All the\n"
"features of the persistent volume are optional and need to be explicitly\n"
"activated. Only the files and folders that you specify are saved.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"open_other_systems\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Opening the persistent volume from other operating systems\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"**It is possible to\n"
"open the persistent volume from other operating systems. But, doing so might\n"
"compromise the security provided by Tails.**\n"
msgstr ""

#. type: Plain text
msgid ""
"For example, image thumbnails might be created and saved by the other "
"operating system. Or, the contents of files might be indexed by the other "
"operating system."
msgstr ""

#. type: Plain text
msgid ""
"Other operating systems should probably not be trusted to handle sensitive "
"information or leave no trace."
msgstr ""
