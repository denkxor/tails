Send an email to <tails-accounting@boum.org>.

Include as much of the following information as you want:

<ul>
  <li class="apply-cv">
    <p>CV</p>
    <p>Feel free to not include your legal name, gender, or photo in your CV.</p>
  </li>
  <li class="apply-cover-letter">
    <p>Cover letter</p>
  </li>
  <li class="apply-contributions">
    <p>Links to other contributions to free software, GitHub, or GitLab accounts</p>
  </li>
  <li class="apply-portfolio">
    <p>Links to your personal website and portfolio</p>
  </li>
  <li class="references">
    <p>References</p>
  </li>
</ul>

**Mention the name of the job offer clearly in the body of your email.**
