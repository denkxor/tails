[[!meta title="Media appearances in 2019"]]

* 2019-10-24: ris [announced the release of Tails
  4.0](https://lwn.net/Articles/802876/) in Linux Weekly News.

* 2019-08-24: Edward Snowden [writes on
  Twitter](https://twitter.com/Snowden/status/1165297667490103302):
  <em>“In 2013, when a small team of journalists and I went head to
  head against @NSAGov to reveal the secret system of global mass
  surveillance, we used @Tails_Live to communicate—to reduce the risk
  of basic but deadly mistakes.<br/>
  The NSA only learned of our plan when it hit the news.”</em>

* 2019-08-04: In [Attorney for Daniel Hale blasts indictment for leaking
  classified drone documents](https://www.dailydot.com/layer8/daniel-hale-indictment/),
  David Gilmour from The Daily Dot explains that the indictment of Daniel Hale
  for the leaks of the [Drone Papers](https://theintercept.com/drone-papers/)
  focuses his use of anonymity tools like Tails and Tor. According to Hale's
  attorneys Jesselyn Radack <em>“These cases are criminalizing both
  whistleblowing and journalistic best practices like using encryption and
  protecting source anonymity”</em>.

* 2019-01-30: [Want a bit of privacy? Got a USB stick? Welcome to TAILS
  3.12](https://www.theregister.co.uk/2019/01/30/tails_3_12/) by Richard
  Speed in The Register.

* 2019-01-21: [Tails 3.12 fait évoluer sa méthode d'installation : quels changements concrets ?](https://www.nextinpact.com/news/107525-tails-3-12-fait-evoluer-sa-methode-dinstallation-quels-changements-concrets.htm) by David Legrand in Next INpact.
